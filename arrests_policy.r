# Author : Georgios Spyrou

# Loading the libraries we are gonna need
librar_list = c("dplyr","data.table","ggplot2")
install.packages(librar_list)

library(dplyr)
library(data.table)
library(ggplot2)

# Part 1 - Data Management

# Importing our data
case_data = read.csv("case.csv",header = TRUE)%>%as.data.frame()
demo_data = read.csv("demo.csv",header = TRUE)%>%as.data.frame()
prior_arr_data = read.csv("prior_arrests.csv",header = TRUE)%>%as.data.frame()

# 1)
# Creating the re_arrest variable containing information about if the defendant was arrested again
# prior to trial
newdata = case_data%>%group_by(person_id)%>%arrange(arrest_date)%>%
  mutate(re_arrest = ifelse(as.Date(arrest_date) < lag(as.Date(dispos_date),1),1,0))

# This is replacing the NA's that we got due to the fact that for every person_id the first case
# has no prior
newdata$re_arrest[is.na(newdata$re_arrest)] = 0

# 2)
# Creating variable that equals the # of arrests prior to the current arrest

old_arrests = prior_arr_data%>%group_by(person_id)%>%
  summarise('Number of Arrests Before' = n())

all_arrests = merge(newdata,old_arrests,by="person_id",all.x = TRUE)

# Some people had no prior arrests so we replace the NA's with 0
all_arrests$`Number of Arrests Before`[is.na(all_arrests$`Number of Arrests Before`)] = 0

# We can check if we did it okay i.e. no NA's left in the dataset
apply(all_arrests, 2, function(x) any(is.na(x)))

final_arrests = all_arrests%>%group_by(person_id)%>%
  arrange(arrest_date)%>%mutate(indx = row_number()-1)%>%
  transform(prior_arrests =`Number of Arrests Before`+indx)

# 3)
# Creating an 'age' variable that equals the defendant's age at the time of each arrest

bd_data = demo_data%>%select(person_id,bdate,race,gender)

bd_data = merge(final_arrests,bd_data,by="person_id",all.x = TRUE)%>%arrange(arrest_date) 

find_age = as.data.frame(apply(bd_data,1,function(x){
  round(as.integer(as.Date(x[["arrest_date"]])-as.Date(x[["bdate"]]))/365)}))

bd_data_final = cbind(bd_data,find_age)
colnames(bd_data_final)[13] = "age"

# Finally we need to create a .csv file containing this final table
# as we need it for submission

write.csv(bd_data_final, file = "gspyrou_final_dataset_CPL.csv", row.names=FALSE)


# Part 2 - Statistical Analysis

# We import the data tha we constructed at the first part
# I have included this file in my submission as asked from Nathan

full_table = read.csv("gspyrou_final_dataset_CPL.csv",header = TRUE)%>%as.data.frame()

supp = full_table%>%group_by(person_id)%>%arrange(arrest_date)%>%summarize(sum_treat = sum(treat))

full_table = merge(full_table,supp,by="person_id",all.x = TRUE)%>%arrange(arrest_date)


# Subset data to people that have got the treatment at least once
all_d = full_table%>%filter(sum_treat != 0)%>%group_by(person_id)%>%
                  mutate(after_arrests = max(indx),before_arrests = min(prior_arrests))

table1 = as.data.table(all_d)
setkey(table1, person_id)

support_table = table1[J(unique(person_id)), mult ='first']%>%
  select(person_id,before_arrests,after_arrests,gender,race)%>%as.data.frame()

# Now we are able to perform a Sample t-test
t.test(support_table$before_arrests,support_table$after_arrests, mu = 0 , alternative = "greater",paired = TRUE)

# And so we see that the p value is almost 0 and hence we reject the Ho hypothesis
# that there is no difference in average arrest rates before and after an individual enters the pilot program


# Visualization of our data

# Calculating differences before-after pilot program
support_table$diff = support_table$before_arrests - support_table$after_arrests

# Histograms
the_histogram_diff = ggplot(support_table, aes(x=diff))+ 
  geom_histogram(colour = "black", fill = "brown",bins = max(support_table$diff))+theme_gray()+
  labs(x='Number of Arrests' , y = "Frequency" , title="Histogram on the difference of Arrests (Before - After Program)")

the_histogram_diff

the_histogram_prior = ggplot(support_table, aes(x=before_arrests))+ 
  geom_histogram(colour = "black", fill = "brown",bins = max(support_table$before_arrests))+theme_gray()+
  labs(x='Number of Arrests' , y = "Frequency" , title="Histogram of Arrests prior Pilot Program")

the_histogram_prior

the_histogram_after = ggplot(support_table, aes(x=after_arrests))+ 
  geom_histogram(colour = "black", fill = "brown",bins = max(support_table$after_arrests))+theme_gray()+
  labs(x='Number of Arrests' , y = "Frequency" , title="Histogram of Arrests after Pilot Program")

the_histogram_after

# Boxplots
ggplot(support_table,aes(x = race,y = before_arrests),fill = race)+geom_boxplot()+
  theme_gray()+labs(x = "Race", y = "Times Arrested before Treatment")

ggplot(support_table,aes(x = race,y = after_arrests),fill = race)+geom_boxplot()+theme_gray()+
  labs(x = "Race", y = "Times Arrested after Treatment")

# Barplot
counts = table(full_table$treat, full_table$race)

barplot(counts, main="Treatment Distribution by Race",
        xlab="Race", col=c("darkblue","red"),
        legend = c("Treatment", "No Treatment"))
