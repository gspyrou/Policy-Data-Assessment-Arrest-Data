# Policy-Data-Assessment-Arrest-Data
Data management and statistical analysis on Los Angeles Arrests data

Our main goal of this project was to determine if the pilot program(policy) should be continued/expanded by estimating its effect
or re-arrest prior to disposition.For this purpose, after the data management part (cleaning of our data,transformations etc) , we chose to evaluate the policy by using a Paired t-test on the difference between the average arrest rate before and after the pilot program has started.Our results indidcated that the pilot program does seem to have a positive effect(reducing arrest rates) for individuals who have been arrested for misdemeanor offenses during a substance abuse.
